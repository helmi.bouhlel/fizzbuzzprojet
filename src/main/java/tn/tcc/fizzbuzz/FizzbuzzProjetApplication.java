package tn.tcc.fizzbuzz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FizzbuzzProjetApplication {

	public static void main(String[] args) {
		SpringApplication.run(FizzbuzzProjetApplication.class, args);
	}

}
