package tn.tcc.fizzbuzz;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class DemoController {

	/**
	 * 
	 * @param x
	 * @return List array String
	 * @throws SaisieErroneeException 
	 */
	@GetMapping("/fizzbuzz/{x}")
	public List<String> fizzbizz (@PathVariable Long x) throws SaisieErroneeException {
		List<String> list = new ArrayList<>();
		// si l'entier est inférieur ou égal à 1 retourne message
		if (x < 1) {
			throw new SaisieErroneeException("entier doit etre supperieur a 1");
		}
		for (int i = 1; i < x+1; i++) {
			// on doit traiter la condition ou la valeur est divisable par 3 et 5 a la fois
			if (i % 3 == 0 && i % 5 == 0) {
				list.add("fizzbizz");

			} if (i % 3 == 0) {
				//si divisable par 3
				list.add("fizz"); 

			} else if (i % 5 == 0) {
				// si divisable par 5
				list.add("bizz"); 
			} else {
				// else ajouter l entier
				list.add(Integer.toString(i)); 
			}
		}
		return list;
	}

}
