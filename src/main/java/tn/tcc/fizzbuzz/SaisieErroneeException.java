package tn.tcc.fizzbuzz;

public class SaisieErroneeException extends Exception {

	public SaisieErroneeException() {
		super();
	}

	public SaisieErroneeException(String s) {
		super(s);
	}
}
